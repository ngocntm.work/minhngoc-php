-- Adminer 4.8.1 MySQL 5.7.36 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `departments` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'Nhân sự',	NULL,	NULL),
(2,	'Marketing',	NULL,	NULL),
(3,	'Tài chính',	NULL,	NULL),
(4,	'Kinh doanh',	NULL,	NULL),
(5,	'Tư vấn',	'2022-01-03 08:18:36',	'2022-01-03 08:18:36');

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1,	'admin',	NULL,	NULL),
(2,	'user',	NULL,	NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `departement_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `departement_id` (`departement_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`departement_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `fullname`, `username`, `password`, `avatar`, `departement_id`, `role_id`, `created_at`, `updated_at`) VALUES
(2,	'nguyễn thế minh ngọc',	'minhngoc',	'e10adc3949ba59abbe56e057f20f883e',	NULL,	1,	1,	NULL,	NULL),
(4,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(5,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(6,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(7,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(8,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(9,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(10,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(11,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(12,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(13,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(14,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(15,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(16,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(17,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(18,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(19,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(20,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(21,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(22,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(23,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(24,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(25,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(26,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(27,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(28,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(29,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(30,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(31,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(32,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(33,	'ádasda',	'aasdas',	'123456',	'ádas',	1,	2,	NULL,	NULL),
(34,	'ádasda',	' aasdas a',	' 123456 ',	'ádas',	1,	2,	NULL,	NULL),
(35,	'â',	' minhngocâ ',	' 12346 ',	'1640709042.png',	4,	2,	NULL,	NULL),
(36,	'adsda',	'  minhngocaa  ',	'  123456  ',	'1640709028.png',	4,	2,	NULL,	NULL);

-- 2022-01-03 08:21:28