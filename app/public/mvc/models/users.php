<?php
class users extends DB
{
    //Lấy user đăng nhập
    public function getUserLogin($username, $password)
    {
        $username = addslashes($username);
        $password = addslashes(md5($password));
        $qr = "SELECT * FROM users where username ='" . $username . "' and password = '" . $password . "'";
        $result = mysqli_query($this->con, $qr);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $user['id'] = $row['id'];
                $user['username'] = $row['username'];
                $user['role_id'] = $row['role_id'];
            }
            return $user;
        } else {
            return false;
        }
    }
    //model lấy danh sach nhân viên
    public function getUsers($item_per_page, $offset)
    {
        $qr = "SELECT users.id as userid, users.fullname as fullname,users.avatar as avatar, users.username as username, departments.name as depart_name FROM users
        INNER JOIN departments on departments.id = users.departement_id 
        ORDER BY users.id DESC
        LIMIT " . $item_per_page . "
        OFFSET " . $offset . "";
        $users =  mysqli_query($this->con, $qr);
        return $users;
    }
    //model thêm nhân viên
    public function addUser($fullname, $username, $password, $avatar, $department_id)
    {
        $role_id = 2;
        $qr = $this->con->prepare("SELECT * FROM users WHERE username=?");
        $qr->bind_param("s", $username);
        $qr->execute();
        $result = $qr->get_result();
        $result = $result->num_rows;
        if ($result == 0) {
            $qr = $this->con->prepare("INSERT INTO users(fullname, username, password, avatar, departement_id, role_id) VALUES(?, ?, ?, ?, ?, ?)");
            $qr->bind_param("ssssii", $fullname, $username, $password, $avatar, $department_id, $role_id);
            $qr->execute();
            return true;
        } else return false;
    }
    //model xoá nhân viên
    public function deleteUser($user_id)
    {
        $qr =  "DELETE FROM users WHERE id = '" . $user_id . "'";
        $result = mysqli_query($this->con, $qr);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    //model lấy thông tin 1 nhân viên
    public function getUser($user_id)
    {
        $qr = "SELECT users.id as userid, users.fullname as fullname,users.avatar as avatar, users.username as username, 
        departments.name as depart_name, users.password as password, users.departement_id as depart_id, users.role_id as role_id, roles.name as role_name
        FROM users
        INNER JOIN departments on departments.id = users.departement_id
        INNER JOIN roles on roles.id = users.role_id
        WHERE users.id = '" . $user_id . "'";
        $result = mysqli_query($this->con, $qr)->num_rows;
        if ($result == 0) {
            return false;
        } else return
            $result = mysqli_query($this->con, $qr);
    }
    //model lưu thông tin khi sửa nhân viên
    public function saveUser($id, $fullname, $username, $password, $avatar, $department_id, $role_id)
    {
        $qr = "UPDATE users
         SET fullname = '" . $fullname . "', username = '" . $username . "', password = '" . $password . "',
         avatar = '" . $avatar . "', departement_id = '" . $department_id . "', role_id = '" . $role_id . "'
         WHERE id='" . $id . "'";
        return mysqli_query($this->con, $qr);
    }
    //Đếm số lượng nhân viên theo tên
    public function getUsersName($name)
    {
        $qr = "SELECT count(*), id, username FROM users WHERE username ='" . $name . "'
        GROUP BY id";
        return mysqli_query($this->con, $qr);
    }
    //Đếm tổng số page paginate
    public function getTotal($item_per_page)
    {
        $qr = "SELECT * FROM users";
        $totalRecods = mysqli_query($this->con, $qr)->num_rows;
        $totalPages = ceil($totalRecods / $item_per_page);
        return $totalPages;
    }
}
