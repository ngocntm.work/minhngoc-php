<?php
class departments extends DB
{

    //model lấy danh sách depart
    public function getDeparts()
    {
        $qr = "SELECT * FROM departments ORDER BY id DESC";
        return mysqli_query($this->con, $qr);
    }
    //model lấy depart theo id
    public function getDepart($id)
    {
        $qr = $this->con->prepare("SELECT * FROM departments WHERE id = ?");
        $qr->bind_param("i", $id);
        $qr->execute();
        $result = $qr->get_result();
        $count = $result->num_rows;
        if ($count == 0) {
            die;
            return false;
        } else
            return $result;
    }
    //model lưu thông tin phòng ban khi thêm mới
    public function saveDepart($departName)
    {
        $qr = "INSERT INTO `departments` (`name`)VALUES ('" . $departName . "');";
        return mysqli_query($this->con, $qr);
    }

    //lưu thông tin phòng ban khi sửa
    public function saveEditDepart($id, $name)
    {
        $qr = "UPDATE departments
        SET name = '" . $name . "' WHERE id='" . $id . "'";
        return mysqli_query($this->con, $qr);
    }

    //Xoá phòng ban
    public function deleteDepart($id)
    {
        $qr = "DELETE FROM departments WHERE id = '" . $id . "'";
        return mysqli_query($this->con, $qr);
    }
}
