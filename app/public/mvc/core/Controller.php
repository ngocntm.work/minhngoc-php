<?php
class Controller
{
    //gọi model
    public function model($model)
    {
        require_once "./mvc/models/" . $model . ".php";
        return new $model;
    }

    //gọi view
    public function viewPage($view, $data = [])
    {
        require_once "./mvc/views/" . $view . ".php";
    }
}
