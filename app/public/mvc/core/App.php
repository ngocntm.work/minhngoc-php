<?php
class App
{
    protected $controller = 'Home';
    protected $action = 'view';
    protected $param;

    function __construct()
    {
        $arr = $this->urlProcess();
        // var_dump($arr);
        // die;
        //Xử lý Controller 

        if (!isset($arr)) {
            require_once './mvc/controllers/' . $this->controller . '.php';
            $this->controller = new $this->controller;
        } else {
            if (file_exists("./mvc/controllers/" . $arr[0] . ".php")) {
                $this->controller = $arr[0];
                unset($arr[0]);
            }
            require_once './mvc/controllers/' . $this->controller . '.php';
            $this->controller = new $this->controller;
        }

        //Xử lý Action
        if (isset($arr[1])) {
            if (method_exists($this->controller, $arr[1])) {
                $this->action = $arr[1];
            } else {
                $this->action = 'error';
            }
            unset($arr[1]);
        }

        //Xử lý params
        $this->param = $arr ? array_values($arr) : [];

        call_user_func_array([$this->controller, $this->action], $this->param);
    }

    function urlProcess()
    {
        if (isset($_GET['page'])) {
            $url = explode('/', filter_var(trim($_GET['page'], '/')));
            // var_dump($url);
            // die;
            return   $url;
        }
    }
}
