<?php
class DB
{
    public $con;
    protected  $servername = 'mysql';
    protected  $username = 'admin';
    protected  $password = 'admin';
    protected  $db = 'rikkeisoft';

    function __construct()
    {

        $this->con = mysqli_connect($this->servername, $this->username, $this->password);
        mysqli_select_db($this->con, $this->db);
        mysqli_query($this->con, "SET NAMES 'utf8'");
    }
}
