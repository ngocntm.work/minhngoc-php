<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        <?php require_once 'public/css/style.css' ?>
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Document</title>
</head>

<body>
    <div id="container">
        <div id="nav">
            <ul>
                <li><a href="#">Trang chủ</a></li>
                <?php
                if ($_SESSION['user']['role_id'] == 1) { ?>
                    <?php
                    if (strpos($_GET['page'], 'Depart')) {
                    ?>
                        <li style="background:#ECECEC"><a href="/?page=ManageAdmin/showDeparts">Quản lý phòng ban</a></li>
                        <li><a href="/?page=ManageAdmin/showUsers">Quản lý nhân viên</a></li>
                    <?php } else { ?>
                        <li><a href="/?page=ManageAdmin/showDeparts">Quản lý phòng ban</a></li>
                        <li style="background:#ECECEC"><a href="/?page=ManageAdmin/showUsers">Quản lý nhân viên</a></li>
                    <?php } ?>
                <?php } else { ?>
                    <?php
                    if (strpos($_GET['page'], 'showDeparts')) {
                    ?>
                        <li style="background:#ECECEC"><a href="/?page=User/showDeparts">Quản lý phòng ban</a></li>
                        <li><a href="/?page=User/showUsers">Quản lý nhân viên</a></li>
                    <?php } else { ?>
                        <li><a href="/?page=User/showDeparts">Quản lý phòng ban</a></li>
                        <li style="background:#ECECEC"><a href="/?page=User/showUsers">Quản lý nhân viên</a></li>
                    <?php } ?>
                <?php } ?>

            </ul>
        </div>
        <div id="profile-img">
            Xin chào: <?php print_r($_SESSION['user']['username']) ?>
            <?php if ($_SESSION['user']['role_id'] == 1) { ?>
                <button class='btn btn-warning' onclick="javascript:location.href='/?page=ManageAdmin/Logout'">Logout</button>
            <?php } else { ?>
                <button class='btn btn-warning' onclick="javascript:location.href='/?page=User/Logout'">Logout</button>
            <?php } ?>

        </div>
        <div id="content">
            <?php
            require_once './mvc/views/Pages/' . $data['Page'] . '.php';
            ?>
        </div>
    </div>
    <div id="footer"></div>

</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</html>