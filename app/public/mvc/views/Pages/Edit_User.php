<table class="table table-bordered text-center">
</table>
<?php
if ($data['UserData'] != '') { ?>
    <form action="/?page=ManageAdmin/saveEdit" method="Post" enctype="multipart/form-data">
        <?php
        foreach ($data['UserData'] as $key) : ?>
            <input type="text" value="<?= $key['userid'] ?>" name="userid" hidden>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Họ tên đầy đủ</label>
                <input required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="fullname" value="<?= $key['fullname'] ?>">
            </div>

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Nhân viên</label>
                <input required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username" value="<?= $key['username'] ?> ">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Mật khẩu</label>
                <input required type="password" class="form-control" id="exampleInputPassword1" name="password" value="<?= $key['password'] ?> ">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Ảnh đại diện đã up</label>
                <input type="text" value="<?= $key['avatar'] ?>" name="oldavatar" hidden>
                <br>
                <img src="/public/uploads/<?= $key['avatar'] ?>" alt="" width="100" height="100">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Ảnh đại diện mới</label>
                <br>
                <input type="file" name="profileImage" />
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Chọn phòng ban</label>
                <select id="department" name="department_id" class="form-control">
                    <option value="<?= $key['depart_id'] ?>"><?= $key['depart_name'] ?></option>
                    <?php
                    foreach ($data['DepartList'] as $key_2) : ?>{
                    <option value="<?= $key_2['id'] ?>"><?= $key_2['name'] ?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Chức vụ</label>
                <select id="role" name="role" class="form-control">
                    <option value="<?= $key['role_id'] ?>"><?= $key['role_name'] ?></option>
                    <?php
                    foreach ($data['RoleList'] as $key_2) : ?> {
                        <?php if ($key['role_id'] != $key_2['id']) { ?>
                            <option value="<?= $key_2['id'] ?>"><?= $key_2['name'] ?></option>
                        <?php } ?>
                    <?php endforeach; ?>
                </select>
            </div>
        <?php endforeach; ?>
        <button type="submit" class="btn btn-success" style="padding-right:40">Lưu</button>
    </form>
<?php } else echo 'Không tồn tại user' ?>

<!-- scipt khoá edit role người đang login -->
<script>
    window.onload = function() {
        var x = document.getElementById("role").value;
        var role_log = <?php echo $_SESSION['user']['id']; ?>;
        var role_data = <?php echo $data['UserData'][0]['userid'] ?>;
        if (role_log == role_data) {
            document.getElementById("role").disabled = true;
        }
    }
</script>