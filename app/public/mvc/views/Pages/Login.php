<?php
if (isset($_SESSION['user']['role_id'])) {
    if ($_SESSION['user']['role_id'] == 1) {
        echo "<script>
        location.replace('/?page=ManageAdmin/showDeparts');
        </script>";
    }
    else{
        echo "<script>
        location.replace('/?page=User/showDeparts');
        </script>";
    }
}
?>
<h1 style="text-align: center;">Đăng nhập</h1>
<br>
<div class="login-page d-flex justify-content-center">
    <div class="form ">
        <form class="login-form" action="?page=Home/checkUser" method="POST">
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email</label>
                <input type="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username" value="">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Mật khẩu</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" value="">
            </div>
            <button type="submit" class="btn btn-primary">Đăng nhập</button>
        </form>
        <br>
        Chưa có tài khoản? <a href="?page=Home/register">Đăng ký</a>
    </div>
</div>