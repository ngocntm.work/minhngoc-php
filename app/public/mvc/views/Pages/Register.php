    <!--Source frome https://www.w3schools.com/howto/howto_css_signup_form.asp-->
    <form action="">
        <div class="container" style="max-width: 756px; margin:auto">
            <h1 style="text-align: center;">Form Đăng Ký</h1>
            <p>Xin hãy nhập biểu mẫu bên dưới để đăng ký.</p>
            <hr>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Họ tên đầy đủ</label>
                <input required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="fullname" value="">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Tên đăng nhập</label>
                <input type="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username" value="">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Mật khẩu</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="password" value="">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Xác nhận mật khẩu</label>
                <input type="password" class="form-control" id="exampleInputPassword1" name="re-password" value="">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Chọn phòng ban</label>
                <select id="department" name="department_id" class="form-control">
                    <?php
                    foreach ($data['DepartList'] as $key) : ?>{
                    <option value="<?= $key['id'] ?>"><?= $key['name'] ?></option>
                <?php endforeach; ?>
                </select>
            </div>
            <p class="message">Already registered? <a href="/?page=ManageAdmin">Đăng nhập</a></p>
            <div class="clearfix">
                <button type="submit" class="btn btn-primary">Đăng ký</button>
            </div>
        </div>
    </form>