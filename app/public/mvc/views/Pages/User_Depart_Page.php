<?php
//Kiểm tra xem cần hiển thị phần nào Depart ?
if ($data['Function'] == 'Depart') { ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Tên phòng ban</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data['Depart_List'] as $key) : ?>
                <tr>
                    <td> <?= $key['name'] ?> </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table><?php
    //Kiểm tra xem cần hiển thị phần nào User ?

        } elseif ($data['Function'] == "User") { ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">Họ tên đầy đủ</th>
                <th scope="col">Tên đăng nhập</th>
                <th scope="col">Ảnh đại diện</th>
                <th scope="col">Phòng ban</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($data['UserList'] as $key) : ?>
                <tr>
                    <td> <?= $key['fullname'] ?> </td>
                    <td> <?= $key['username'] ?> </td>
                    <td> <img src="/public/uploads/<?= $key['avatar'] ?>" alt="" width="300" height="300"> </td>
                    <td> <?= $key['depart_name'] ?> </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <ul class="pagination" style="justify-content: center">
        <?php
            for ($num = 1; $num <= $data['TotalPage']; $num++) { ?>
            <?php
                if ($num == $data['CurentPage']) {
            ?>
                <li class="page-item active" aria-current="page">
                    <a class="page-link" href="/?page=User/showUsers/curent_page=<?= $num ?> "><?= $num ?></a>
                </li>
            <?php } else { ?>
                <li class="page-item">
                    <a class="page-link" href="/?page=User/showUsers/curent_page=<?= $num ?> "><?= $num ?></a>
                </li> <?php } ?>
        <?php
            }
        ?>
    </ul>
<?php } ?>