<div class="add-btn"><button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Thêm
    </button>
</div>
<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">Tên phòng ban</th>
            <th colspan="2" style="width: 10%; text-align: center;">Chức năng</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data['Depart_List'] as $key) : ?>
            <tr>
                <td> <?= $key['name'] ?> </td>
                <td> <a href="/?page=ManageAdmin/editDepart/<?php echo $key['id'] ?>" class="btn btn-secondary">Sửa</a></td>
                <form action="/?page=ManageAdmin/deleteDepart/" method="POST">
                    <input type="text" hidden value="<?= $key['id'] ?>" name="departID">
                    <td> <button class="btn btn-danger" onclick="return confirm('Có chắc muốn xoá?')">Xoá</button></td>
                </form>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<!-- Modal Thêm -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm phòng ban</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/?page=ManageAdmin/addDepart" method="Post">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Tên phòng ban</label>
                        <input required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="departName" value="">
                    </div>
                    <button type="submit" class="btn btn-success" style="float: right;">Thêm</button>
                </form>
            </div>
        </div>
    </div>
</div>