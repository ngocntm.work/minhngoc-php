<div class="add-btn"><button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal">
        Thêm
    </button>
</div>
<div class="col">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th scope="col">STT</th>
                <th scope="col">Ảnh đại diện</th>
                <th scope="col">Họ tên đầy đủ</th>
                <th scope="col">Tên đăng nhập</th>
                <th scope="col">Phòng ban</th>
                <th colspan="2" style="width: 10%; text-align: center;">Chức năng</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = ($data['CurentPage'] - 1) * 10 + 1;
            foreach ($data['UserList'] as $key) : ?>
                <tr>
                    <td><?php echo $i;
                        $i++ ?> </td>
                    <td> <img src="/public/uploads/<?= $key['avatar'] ?>" alt="" width="100" height="100"> </td>
                    <td> <?= $key['fullname'] ?> </td>
                    <td> <?= $key['username'] ?> </td>
                    <td> <?= $key['depart_name'] ?> </td>
                    <!-- <td> <a href="/?page=ManageAdmin/editUser/<?php echo $key['userid'] ?>" class="btn btn-secondary">Sửa</a></td> -->
                    <td><button class="btn btn-info editbtn" value="<?= $key['userid'] ?>">Sửa</button>
                    </td>
                    <form action="/?page=ManageAdmin/deleteUser/" method="POST" id="delete-form">
                        <input type="text" hidden value="<?= $key['userid'] ?>" name="userid">
                        <td> <button class="btn btn-danger" onclick="return confirm('Có chắc muốn xoá?')">Xoá</button></td>
                    </form>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<!-- Pagination -->
<ul class="pagination" style="justify-content: center">
    <?php
    for ($num = 1; $num <= $data['TotalPage']; $num++) { ?>
        <?php if ($num == $data['CurentPage']) { ?>
            <li class="page-item active" aria-current="page">
                <a class="page-link" href="/?page=ManageAdmin/showUsers/curent_page=<?= $num ?> "><?= $num ?></a>
            </li>
        <?php } else { ?>
            <li class="page-item">
                <a class="page-link" href="/?page=ManageAdmin/showUsers/curent_page=<?= $num ?> "><?= $num ?></a>
            </li>
        <?php } ?>
    <?php } ?>
</ul>
<!-- Modal Thêm -->
<div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm nhân viên</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/?page=ManageAdmin/addUser" method="Post" enctype="multipart/form-data" id="addForm">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Họ tên đầy đủ</label>
                        <input required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="fullname" value="">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Nhân viên</label>
                        <input required type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="username" value="">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Mật khẩu</label>
                        <input required type="password" class="form-control" id="exampleInputPassword1" name="password" value="">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Ảnh đại diện</label>
                        <input type="file" name="profileImage" />
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Chọn phòng ban</label>
                        <select id="department" name="department_id" class="form-control">
                            <?php
                            foreach ($data['DepartList'] as $key) : ?>{
                            <option value="<?= $key['id'] ?>"><?= $key['name'] ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-success" style="float: right;">Thêm</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal sửa -->
<div class="modal fade" id="editModel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form id="editForm" action="/?page=ManageAdmin/saveEdit" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sửa nhân viên</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body ">
                    <div class="wall-post row">
                        <div class="col-md-6">
                            <input type="text" value="<?= $key['userid'] ?>" name="userid" id="userid" hidden>
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Họ tên đầy đủ</label>
                                <input required type="text" class="form-control" id="fullname" aria-describedby="emailHelp" name="fullname" value="">
                            </div>

                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nhân viên</label>
                                <input required type="text" class="form-control" id="username" aria-describedby="emailHelp" name="username" value="">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Mật khẩu</label>
                                <input required type="password" class="form-control" id="password" name="password" value="">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Chọn phòng ban</label>
                                <select id="departmentlist" name="department_id" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 ms-auto">
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Ảnh đại diện đã up</label>
                                <input type="text" value="" name="oldavatar" id="oldavatar" hidden>
                                <br>
                                <img src="" alt="" width="100" height="100" id="old-img">
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Ảnh đại diện mới</label>
                                <br>
                                <input type="file" name="profileImage" id="profileImage" />
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label">Chức vụ</label>
                                <select id="role" name="role" class="form-control">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Huỷ</button>
                    <button type="submit" class="btn btn-primary editsave">Lưu thay đổi</button>
                </div>
        </div>
        </form>

    </div>
</div>
<!-- Modal sửa thành công -->
<div class="modal fade" id="success" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #d1e7dd; color: #0f5132; border-color: #badbcc;">
                <h5 class="modal-title" id="successMessage"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
        </div>
    </div>
</div>
<!-- Modal lỗi img -->
<div class="modal fade" id="error-img" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #f8d7da; color: #842029; border-color: #f5c2c7;">
                <h5 class="modal-title" id="errorMessage"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="closeError"></button>
            </div>
        </div>
    </div>
</div>

<script>
    // Hiện modal sửa
    $(".editbtn").click(function() {
        var userid = $(this).val();
        $.ajax({
            url: '/?page=ManageAdmin/editUser/' + userid,
            type: 'GET',
            dataType: 'json',
            data: {}
        }).done(function(ketqua) {
            var departList = ketqua['DepartList'];
            var roleList = ketqua['RoleList'];
            var departSelect = $('<select>');
            var roleSelect = $('<select>');
            var id = <?php echo $_SESSION['user']['id']; ?>;
            $("#editModel").modal("show");
            $(".modal-body #userid").val(ketqua['UserData'][0]);
            $(".modal-body #fullname").val(ketqua['UserData'][1]);
            $(".modal-body #username").val(ketqua['UserData'][3]);
            $(".modal-body #password").val(ketqua['UserData'][5]);
            $(".modal-body #oldavatar").val(ketqua['UserData'][2]);
            $("#old-img").attr("src", "public/uploads/" + ketqua['UserData'][2]);
            $('#departmentlist').find('option').remove().end();
            $('#role').find('option').remove().end();
            $('#role').removeAttr('disabled');

            if (id == ketqua['UserData'][0]) {
                $('#role').attr('disabled', 'disabled');
            }
            $.each(departList, function(index, value) {
                if (ketqua['UserData'][6] == value['id']) {
                    departSelect.append($('<option selected="selected"></option>').val(value['id']).html(value['name']));
                } else {
                    departSelect.append($('<option></option>').val(value['id']).html(value['name']));
                }
            });
            $('#departmentlist').append(departSelect.html());

            $.each(roleList, function(index, value) {
                if (ketqua['UserData'][7] == value['id']) {
                    roleSelect.append($('<option selected="selected"></option>').val(value['id']).html(value['name']));
                } else {
                    roleSelect.append($('<option></option>').val(value['id']).html(value['name']));
                }
            });
            $('#role').append(roleSelect.html());
        });
    });
    // Sửa nhân viên
    $(document).on('submit', '#editForm', function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function(data) {
            console.log(data);
            if (data == 1) {
                $('#editModel').modal('hide');
                $(".modal-body").val($("#successMessage").text('Sửa thành công'));

                $('#success').modal('show');
                setTimeout(function() {
                    window.location.reload();
                }, 3000);
            }
            if (data == 0) {
                $('#editModel').modal('hide');
                $(".modal-body").val($("#errorMessage").text('Ảnh không đúng định dạng'));
                $('#error-img').modal('show');
                $("#error-img").on('hidden.bs.modal', function(e) {
                    $('#editModel').modal('show');
                });
            }
            if (data == 2) {
                $('#editModel').modal('hide');
                $(".modal-body").val($("#errorMessage").text('Không sửa được'));
                $('#error-img').modal('show');
                $("#error-img").on('hidden.bs.modal', function(e) {
                    $('#editModel').modal('show');
                });
            }
            if (data == 3) {
                $('#editModel').modal('hide');
                $(".modal-body").val($("#errorMessage").text('Nhân viên đã tồn tại'));
                $('#error-img').modal('show');
                $("#error-img").on('hidden.bs.modal', function(e) {
                    $('#editModel').modal('show');
                });
            }
        });
    });

    // Thêm user
    $(document).on('submit', '#addForm', function(e) {
        e.preventDefault();
        console.log(e);
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function(data) {
            console.log(data);
            if (data == 1) {
                $('#addModal').modal('hide');
                $(".modal-body").val($("#successMessage").text('Thêm thành công'));
                $('#success').modal('show');
                setTimeout(function() {
                    window.location.reload();
                }, 3000);
            }
            if (data == 2) {
                $('#addModal').modal('hide');
                $(".modal-body").val($("#errorMessage").text('Ảnh không đúng định dạng'));
                $('#error-img').modal('show');
                $("#error-img").on('hidden.bs.modal', function(e) {
                    $('#addModal').modal('show');
                });
            }
            if (data == 0) {
                $('#addModal').modal('hide');
                $(".modal-body").val($("#errorMessage").text('Nhân viên đã tồn tại'));
                $('#error-img').modal('show');
                $("#error-img").on('hidden.bs.modal', function(e) {
                    $('#addModal').modal('show');
                });
            }
        });
    });
    // Xoá user
    $(document).on('submit', '#delete-form', function(e) {
        e.preventDefault();
        console.log(e);
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false
        }).done(function(data) {
            console.log(data);
            if (data == 1) {
                $(".modal-body").val($("#successMessage").text('Xoá thành công'));
                $('#success').modal('show');
                setTimeout(function() {
                    window.location.reload();
                }, 3000);
            } else {
                $(".modal-body").val($("#errorMessage").text('Xoá không thành công'));
                $('#error-img').modal('show');
            }
        });
    });
</script>