<?php
class Home extends Controller
{


    // Action trả về page đăng nhập - Page mặc định
    function view()
    {
        $user = $this->model('users');
        $this->viewPage('Layout', [
            'Page' => 'Login',
            // 'user' => $user->getUser()
        ]);
    }

    //Kiểm trả user khi login
    function checkUser()
    {
        $model = $this->model('users');
        if (isset($_POST['username'])) {
            $check = $model->getUserLogin($_POST['username'], $_POST['password']);
        }
        if ($check) {
            if ($check['role_id'] == 1) {
                $_SESSION['user'] = $check;
                header('Location:/?page=ManageAdmin/showDeparts');
            } else {
                $_SESSION['user'] = $check;
                header('Location:/?page=User/showDeparts');
            }
        } else {
            echo "<script>alert('Sai tên đăng nhập hoặc mật khẩu');
             location.replace('/');
             </script>";
        }
    }

    // Action trả về page đăng ký
    function register()
    {
        $model = $this->model('users');
        $data_depart = $model->getDeparts();
        $row_depart = $data_depart->fetch_all(MYSQLI_ASSOC);
        $this->viewPage('Layout', [
            'Page' => 'Register',
            'DepartList' => $row_depart
        ]);
    }
}
