<?php
class User extends Controller
{
    function __construct()
    {
        if (!isset($_SESSION['user'])) {
            echo "<script>alert('Cần đăng nhập để thực hiện chức năng');
            location.replace('?page=Home');
            </script>";
        }
    }

    function showDeparts()
    {
        $model = $this->model('department');
        $data = $model->getDeparts();
        $row = $data->fetch_all(MYSQLI_ASSOC);
        $this->viewPage('Manage_Layout', [
            'Page' => 'User_Depart_Page',
            'Depart_List' => $row,
            'Function' => 'Depart'
        ]);
    }

    function showUsers()
    {
        $item_per_page = 10;
        $curent_page = substr($_GET['page'], -1);
        if ($curent_page == 's') {
            $curent_page = 1;
        }
        $offset = ($curent_page - 1) * $item_per_page;
        $model = $this->model('users');
        $data = $model->getUSers($item_per_page, $offset);
        $row = $data->fetch_all(MYSQLI_ASSOC);
        $totalPage = $model->getTotal($item_per_page);
        $data_depart = $model->getDeparts();
        $row_depart = $data_depart->fetch_all(MYSQLI_ASSOC);
        $this->viewPage('Manage_Layout', [
            'Page' => 'User_Depart_Page',
            'UserList' => $row,
            'DepartList' => $row_depart,
            'TotalPage' => $totalPage,
            'CurentPage' => $curent_page,
            'Function' => 'User'
        ]);
    }

    function Logout()
    {
        session_destroy();
        echo "<script>;
        location.replace('/');
        </script>";
    }
}
