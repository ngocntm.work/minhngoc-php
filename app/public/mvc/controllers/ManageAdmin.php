<?php
class ManageAdmin extends Controller
{

    function __construct()
    {
        if (!isset($_SESSION['user'])) {
            echo "<script>alert('Cần đăng nhập để thực hiện chức năng');
            location.replace('?page=Home');
            </script>";
        }
        if ($_SESSION['user']['role_id'] == 2) {
            echo "<script>alert('Không có quyền thực hiện hành động');
            location.replace('?page=Home');
            </script>";
        }
    }
    //Action trả về page 404 khi URL sai
    function error()
    {
        $this->viewPage('Layout', [
            'Page' => '404'
        ]);
    }


    //Action hiển thị danh sách phòng ban
    function showDeparts()
    {
        $model = $this->model('departments');
        $data = $model->getDeparts();
        $row = $data->fetch_all(MYSQLI_ASSOC);

        $this->viewPage('Manage_Layout', [
            'Page' => 'Manage_Depart',
            'Depart_List' => $row,
        ]);
    }

    //Action hiển thị danh sach user
    function showUsers()
    {
        $item_per_page = 10;
        $curent_page = substr($_GET['page'], -1);
        if ($curent_page == 's') {
            $curent_page = 1;
        }
        $offset = ($curent_page - 1) * $item_per_page;
        $model = $this->model('users');
        $data = $model->getUSers($item_per_page, $offset);
        $row = $data->fetch_all(MYSQLI_ASSOC);
        $totalPage = $model->getTotal($item_per_page);
        $model = $this->model('departments');
        $data_depart = $model->getDeparts();
        $row_depart = $data_depart->fetch_all(MYSQLI_ASSOC);
        $this->viewPage('Manage_Layout', [
            'Page' => 'Manage_User',
            'UserList' => $row,
            'DepartList' => $row_depart,
            'TotalPage' => $totalPage,
            'CurentPage' => $curent_page,
        ]);
    }

    //action thêm user
    function addUser()
    {
        $fullname = $_POST['fullname'];
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        $department_id = $_POST['department_id'];
        $upload = $this->uploadFile($_FILES);
        if (!$upload) {
            echo "2";
        } else {
            $model = $this->model('users');
            $result = $model->addUser($fullname, $username, $password, $upload, $department_id);
            if ($result) {
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    //action xoá user
    function deleteUser()
    {
        $userid = $_POST['userid'];
        $model = $this->model('users');
        $result = $model->deleteUser($userid);
        if ($result) {
            $check = 1;
            echo json_encode($check);
        } else {
            $check = 0;
            echo json_encode($check);
        }
    }

    //action gọi page sửa user
    function editUser($userId)
    {
        $model = $this->model('users');
        $result = $model->getUser($userId);
        $model = $this->model('roles');
        $list_role = $model->getRole();
        $list_role = $list_role->fetch_all(MYSQLI_ASSOC);
        if ($result) {
            $row = $result->fetch_row();
            $model = $this->model('departments');
            $data_depart = $model->getDeparts();
            $row_depart = $data_depart->fetch_all(MYSQLI_ASSOC);
            $data['UserData'] = $row;
            $data['DepartList'] = $row_depart;
            $data['RoleList'] =  $list_role;
            echo json_encode($data);
        } else {
            echo "<script>alert('Không tồn tại nhân viên cần sửa');
            location.replace('/?page=ManageAdmin/showUsers');
            </script>";
        }
    }

    //action lưu thông tin khi sửa user
    function saveEdit()
    {
        $id = $_POST['userid'];
        $fullname = $_POST['fullname'];
        $username = $_POST['username'];
        $password = md5($_POST['password']);
        $department_id = $_POST['department_id'];
        $role = $_POST['role'];
        $model = $this->model('users');
        $check = $model->getUsersName($username)->fetch_row();
        if ($check[0] != 0) {
            if ($check[1] != $id) {
                echo '3';
                exit;
            }
        }
        if ($_FILES['profileImage']['name'] == "") {
            $avatar = $_POST['oldavatar'];
            $result = $model->saveUser($id, $fullname, $username, $password, $avatar, $department_id, $role);
            if ($result) {
                echo "1";
            }
        } else {
            $upload = $this->uploadFile($_FILES);
            if (!$upload) {
                echo "0";
            } else {
                $result = $model->saveUser($id, $fullname, $username, $password, $upload, $department_id, $role);
                if ($result) {
                    echo "1";
                } else {
                    echo "2";
                }
            }
        }
    }
    //action thêm phòng ban
    function addDepart()
    {

        $departName = $_POST['departName'];
        $model = $this->model('departments');
        $result = $model->saveDepart($departName);
        if ($result) {
            echo "<script>alert('Thêm thành công');
            location.replace('/?page=ManageAdmin/showDeparts');
            </script>";
        } else {
            echo "<script>alert('Thêm không thành công');
            location.replace('/?page=ManageAdmin/showDeparts');
            </script>";
        }
    }

    //Sửa phòng ban
    function editDepart($departID)
    {
        $model = $this->model('departments');
        $result = $model->getDepart($departID);
        if ($result) {
            $row = $result->fetch_all(MYSQLI_ASSOC);
            $this->viewPage('Manage_Layout', [
                'Page' => 'Edit_Depart',
                'DepartList' => $row
            ]);
        } else {
            echo "<script>alert('Không tồn tại phòng ban cần sửa');
            location.replace('/?page=ManageAdmin/showDeparts');
            </script>";
        }
    }

    //Lưu lại thông tin đã sửa
    function saveEditDepart()
    {
        $id = $_POST['id'];
        $name = $_POST['departName'];
        $model = $this->model('departments');
        $result = $model->saveEditDepart($id, $name);
        if ($result) {
            echo "<script>alert('Sửa thành công');
            location.replace('/?page=ManageAdmin/showDeparts');
            </script>";
        } else {
            echo "<script>alert('Sửa không thành công');
            location.replace('/?page=ManageAdmin/Depart');
            </script>";
        }
    }

    //Xoá phòng ban
    function deleteDepart()
    {
        $id = $_POST['departID'];
        $model = $this->model('departments');
        $result = $model->deleteDepart($id);
        echo "<script>alert('Xoá thành công');
        location.replace('/?page=ManageAdmin/showDeparts');
        </script>";
    }

    //Logout
    function Logout()
    {
        session_destroy();
        echo "<script>;
        location.replace('/');
        </script>";
    }

    function uploadFile($file)
    {
        $target_dir = "public/uploads/";
        $temp = explode(".", $file["profileImage"]["name"]);

        $newfilename = round(microtime(true)) . '.' . end($temp);
        $target_file = $target_dir . basename($newfilename);
        $flag = true;
        $list_type = array('jpg', 'git', 'png');
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        if (!in_array($imageFileType, $list_type)) {
            $flag = false;
        }

        if ($flag) {
            move_uploaded_file($file["profileImage"]["tmp_name"], $target_file);
            return $newfilename;
        } else {
            return $flag;
        }
    }
}
